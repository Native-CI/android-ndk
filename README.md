# android-ndk

[![status-badge](https://ci.codeberg.org/api/badges/Native-CI/android-ndk/status.svg)](https://ci.codeberg.org/Native-CI/android-ndk)
![arch|amd64](https://img.shields.io/badge/arch-amd64-green)

This Docker image contains the Android SDK and most common packages necessary
for building Android apps in a CI tool like GitLab CI. Make sure your CI
environment's caching works as expected, this greatly improves the build time,
especially if you use multiple build jobs.

*Note:* This was originally forked from [jangrewe/gitlab-ci-android](https://github.com/jangrewe/gitlab-ci-android),
        but has by now diverged.

- It has a full suite of native build tools thanks to being based on
  [Native-CI/cpp-meson](https://codeberg.org/Native-CI/cpp-meson)
- It also has [Android NDK](https://developer.android.com/ndk/) installed.

## Usage

Docker Hub: [nativeci/android-ndk](https://hub.docker.com/r/nativeci/android-ndk/)


```bash
$ docker pull nativeci/android-ndk
```

## Build Instructions

```bash
$ docker network ls
```

Pick a known good network, say `host`. This is a good choice in most cases.

Increment the `.0` in the command below as necesary. The CI will use the build
number here, so if you're building on the same day as the CI, best pick another
pattern.

```bash
$ docker build --network=host \
    --tag=nativeci/android-ndk:release-$(date +%Y%m%d).0 \
    --tag=nativeci/android-ndk:latest \
    .
```

Or see `build.sh` - that simplifies it mildly.

Happy? Push the image with all tags.

```bash
$ docker push -a nativeci/android-ndk
```

# Note

Older builds are pushed to docker hub as `jfinkhaeuser/gitlab-ci-android-ndk`.
